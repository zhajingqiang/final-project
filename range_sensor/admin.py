from django.contrib import admin
from range_sensor.models import Sensor,Sensor_info,Test

admin.site.register(Sensor)
admin.site.register(Sensor_info)
admin.site.register(Test)
# Register your models here.
