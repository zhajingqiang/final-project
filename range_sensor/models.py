from django.db import models

class Sensor(models.Model):
	sensor_name = models.CharField(max_length=200)
	pub_date = models.DateTimeField('sensor added date')
	def __str__(self):
		return self.sensor_name
# Create your models here.
class Sensor_info(models.Model):
	info = models.ForeignKey(Sensor)
	sensor_info_text = models.CharField(max_length=200)
	def __str__(self):
		return self.sensor_info_text

class Test(models.Model):
	test_text = models.CharField(max_length=200)
	test_date = models.DateTimeField('date tested')
	def __str__(self):
		return self.test_text
# Create your models here.
