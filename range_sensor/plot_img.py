import random 
import matplotlib.pyplot as plt
i = 1
xcord = []
ycord = []
while True:
    x =  random.random() *10
    y =  random.random() *10
    print 'Cartesian coordinate', x,y
    if (i%20)==0:
        plt.plot(x,y,'ro')
        plt.xlim([0,10])
        plt.ylim([0,10])
        plt.xlabel('x axis')
        plt.ylabel('y axis')
        plt.title('Map')
        plt.savefig('Map')
        plt.close()
    i +=1
