from django.conf.urls import patterns, url

from range_sensor import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<sensor_id>\d+)/$', views.detail, name='detail'),
    url(r'^download/(?P<sensor_id>\d+)/$', views.download, name='download'),
    url(r'^staticImage.png/(?P<sensor_id>\d+)$', views.showStaticImage),
    url(r'^gyro_test/$', views.gyro),
    url(r'^range_test/$', views.range),
    url(r'^range_gyro_test/$', views.range_gyro),
    url(r'^gyro_test/result.png$', views.plotGyro),
    url(r'^range_test/range.png$', views.plotRange),
    url(r'^range_gyro_test/range_gyro.png$', views.plotRange_gyro),
)