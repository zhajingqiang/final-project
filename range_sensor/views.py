from django.shortcuts import get_object_or_404,render
from django.http import HttpResponse,HttpResponseRedirect,Http404
from range_sensor.models import Sensor,Sensor_info,Test
from django.template import RequestContext, loader
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
import numpy as np
import random
import datetime
import zmq
import time
import sys
import numpy as np
import math 
from scipy.optimize import curve_fit

global xcoor
global ycoor
xcoor=[]
ycoor=[]


def index(request):
	global xcoor
	global ycoor
	xcoor=[]
	ycoor=[]
	latest_sensor_list = Sensor.objects.order_by('-pub_date')[:5]
	template = loader.get_template('range_sensor/index.html')
	context = RequestContext(request, {
        'latest_sensor_list': latest_sensor_list,
    })
	return HttpResponse(template.render(context))
	# return 
def detail(request, sensor_id):
    sensor_info = get_object_or_404(Sensor, pk=sensor_id)
    return render(request, 'range_sensor/detail.html', {'sensor': sensor_info})

def gyro(request):
    return render(request, 'range_sensor/gyro.html')

def range(request):
    return render(request, 'range_sensor/range.html')

def range_gyro(request):
	return render(request, 'range_sensor/range_gyro.html')

def download(request,sensor_id):
	if sensor_id == '3':
		return HttpResponseRedirect('https://docs.google.com/document/d/1Y-yZnNhMYy7rwhAgyL_pfa39RsB-x2qR4vP8saG73rE/edit')
	elif sensor_id =='2':
		return HttpResponseRedirect('http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00036465.pdf')
	else:
		raise Http404

def showStaticImage(request,sensor_id):
	if sensor_id == '2':
		imagePath = "/Users/zhajingqiang/Documents/Learning in Austin/Python/Final project/obstacle/range_sensor/gyro.png"
	elif sensor_id == '3':
		imagePath = "/Users/zhajingqiang/Documents/Learning in Austin/Python/Final project/obstacle/range_sensor/Ultrasonic_sensor.png"
	else:
		raise Http404
	Image.init()
	i = Image.open(imagePath)
	response = HttpResponse(content_type ='image/png')
	i.save(response,'PNG')
	return response

def Map(request):
	imagePath = "/Users/zhajingqiang/Documents/Learning in Austin/Python/Final project/obstacle/range_sensor/Map.png"
	Image.init()
	i = Image.open(imagePath)
	response = HttpResponse(content_type ='image/png')
	i.save(response,'PNG')
	return response

def plotGyro(request):
	port1 = 5556
	topic1 = 42
	# port2 = 5557
	# topic2 = 42
	context = zmq.Context()
	socket1 = context.socket(zmq.SUB)
	socket1.setsockopt(zmq.SUBSCRIBE,"%d" % topic1)
	socket1.connect("tcp://10.145.187.0:%s" % port1)
	# socket2 = context.socket(zmq.SUB)
	# socket2.setsockopt(zmq.SUBSCRIBE,"%d" % topic2)
	# socket2.connect("tcp://10.145.218.44:%s" % port2)
	message1 = socket1.recv()
	new_message1 = message1.split()
	theta = -float(new_message1[1])
	fig = Figure()
	ax=fig.add_subplot(1,1,1,polar=True)
	distance = np.arange(0,5,0.2)
	p =  np.ones(len(distance))*(-theta/180 * math.pi)
	ax.plot(p,distance,color='r',linewidth=3)
	ax.set_rmax(10)
	ax.grid(True)
	canvas = FigureCanvas(fig)
	response = HttpResponse(content_type='image/png')
	canvas.print_png(response)
	return response

def plotRange(request):
	# port1 = 5556
	# topic1 = 42
	port2 = 5557
	topic2 = 42
	context = zmq.Context()
	# socket1 = context.socket(zmq.SUB)
	# socket1.setsockopt(zmq.SUBSCRIBE,"%d" % topic1)
	# socket1.connect("tcp://10.145.218.44:%s" % port1)
	socket2 = context.socket(zmq.SUB)
	socket2.setsockopt(zmq.SUBSCRIBE,"%d" % topic2)
	socket2.connect("tcp://10.145.187.0:%s" % port2)
	# message1 = socket1.recv()
	# new_message1 = message1.split()
	# theta = float(new_message1[1])
	message2 = socket2.recv()
	new_message2 = message2.split()
	distance = float(new_message2[1])
	fig = Figure()
	ax=fig.add_subplot(1,1,1)
	p =  np.arange(-10,10,1)
	q =  np.ones(len(p))*distance
	ax.plot(p,q,color ='r')
	ax.set_xlim([-50, 50])
	ax.set_ylim([0, 100])
	ax.grid(True)
	canvas = FigureCanvas(fig)
	response = HttpResponse(content_type='image/png')
	canvas.print_png(response)
	return response

def plotRange_gyro(request):
	global xcoor
	global ycoor
	port1 = 5556
	topic1 = 42
	port2 = 5557
	topic2 = 42
	context = zmq.Context()
	socket1 = context.socket(zmq.SUB)
	socket1.setsockopt(zmq.SUBSCRIBE,"%d" % topic1)
	socket1.connect("tcp://10.145.187.0:%s" % port1)
	socket2 = context.socket(zmq.SUB)
	socket2.setsockopt(zmq.SUBSCRIBE,"%d" % topic2)
	socket2.connect("tcp://10.145.187.0:%s" % port2)
	message1 = socket1.recv()
	message2 = socket2.recv()
	new_message2 = message2.split()
	new_message1 = message1.split()
	theta = float(new_message1[1])
	distance = float(new_message2[1])
	fig = Figure()
	ax=fig.add_subplot(1,1,1)
	xcoor.append(-distance * math.sin(theta/180 * math.pi))
	ycoor.append(distance * math.cos(theta/180 * math.pi))
	xmin = min(xcoor)
	xmax = max(xcoor)
	if len(xcoor)>10:
		A,B = curve_fit(f, xcoor, ycoor)[0]
		y1 = A * xmin + B
		y2 = A * xmax + B
		ax.plot([xmin,xmax],[y1,y2],color='r')
	ax.scatter(xcoor,ycoor,color='green')
	ax.set_xlim([-20, 20])
	ax.set_ylim([0, 40])
	canvas = FigureCanvas(fig)
	response = HttpResponse(content_type='image/png')
	canvas.print_png(response)
	return response
# def plotResults(request):
# 	# fig = Figure()
# 	# ax=fig.add_subplot(1,1,1)
# 	# p = random.random()*10
# 	# q = random.random()*10
# 	# ax.scatter(p,q,color='green')
# 	# canvas = FigureCanvas(fig)
# 	# response = HttpResponse(content_type='image/png')
# 	# canvas.print_png(response)
# 	# return response
# 	fig=Figure()
# 	ax=fig.add_subplot(111)
# 	x=[]
# 	y=[]
# 	now=datetime.datetime.now()
# 	delta=datetime.timedelta(days=1)
# 	for i in range(10):
# 	    x.append(now)
# 	    now+=delta
# 	    y.append(random.randint(0, 1000))
# 	ax.plot_date(x, y, '-')
# 	ax.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
# 	fig.autofmt_xdate()
# 	canvas=FigureCanvas(fig)
# 	response=HttpResponse(content_type='image/png')
# 	canvas.print_png(response)
# 	return response
# # Create your views here.

def f(x, A, B): # this is your 'straight line' y=f(x)
    return A*x + B